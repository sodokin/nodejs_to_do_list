module.exports = (sequelize, DataTypes) => {
    // Here we have the description of the contents of the model table User
    return sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {msg: 'The lastname can not be null!'},
                notNull: {msg: 'The lastname of the task is a required data!'}
            }
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {msg: 'The lastname can not be null!'},
                notNull: {msg: 'The lastname of the task is a required data!'}
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique:{
                msg:"The email entered is already assigned"
            },
            validate: {
                isEmail: {msg: "Please enter a valid email"},
                notEmpty: {msg: 'The email can not be empty!'},
                notNull: {msg: 'The email of the task is a required data!'}

            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {msg: 'The password cannot be empty!'},
                notNull: {msg: 'The password is a required data!'}

            }
        }
    }
    // , {
    //     timestamps:true,
    //     createdAt:'created',
    //     updateAt: true,
    // }
    )
}

module.exports = ( sequelize, DataTypes ) => {
    // Here we have the description of the contents of the model table
    return sequelize.define('TodoList', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: {
                msg: `This task already exist!. Please try again.`
            },
            validate: {
                notEmpty: {msg: 'The title of the task cannot be empty!'},
                notNull: {msg: 'The title of the task is a required input!'}
            }
        },
        autor: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {msg: `The name of the author of the task is required!`},
                notNull: {msg: `The autor of the task is a required input!`},
            }

        },
        content: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: {msg: 'The content of the task cannot be empty!'},
                notNull: {msg: 'The content of the task is a required input!'}
            }

        }
    }, {
        timestamps:true,
        createdAt:'created',
        updateAt: true,
    }

    )
}
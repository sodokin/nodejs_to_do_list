// This file corresponds to a middleware which will be used to secure the exchanges between the users of the application and the api
const jwt = require('jsonwebtoken');
require('dotenv').config()

// console.log(process.env)

module.exports = (req, res, next) => {
    // Here we get the http header 'authorization' which is the header through which the token sent by the users will transit
    const authorizationHeader = req.headers.authorization
    //  Here we check that the token has been provided
    if(!authorizationHeader) {
        const message = `You did not provide an authentication token. Add one in the request header`
        return res.status(401).json({ message })
    }
    // Here we retrieve the sent token
        const token = authorizationHeader.split(' ')[1]
        const decodedToken = jwt.verify(token, process.env.PRIVATE_KEY, (error,decodedToken) => { // to the 'verify' method we check if the token is valid
            if(error) {
                const message = `The user is not allowed to access this resource`
                return res.status(401).json({ message, data: error })
            }
            // Here we deny access to the user if the token does not match
            const userId = decodedToken.userId
            if(req.body.userId && req.body.userId !== userId) {
                const message = `the identifier is not valid`
                return res.status(401).json({ message })
            } else {
                next() // If all the tests are passed, we authorize the access to the user with the next method
            }
        })
}
const { Task } = require('../../db/sequelize');
const auth = require('../../auth/auth') 


module.exports = (app) => {
    app.get("/api/findOnetask/:id", (req, res) => {
        Task.findByPk(req.params.id)
            .then(task => {
                if (task === null) {
                    const message = "The requested task does not exist. Please create it or select another one.";
                    return res.status(404).json({message});
                }
                const message = `The task ${task.title}  has been found !`
                res.json({message, data:task})
            })
            .catch(error => {
                const message = `The task could not be recovered. Try again in a few moments.`
                res.status(500).json({message, data: error})
            })
    })
};
// End-point display of tasks
const { Task } = require('../../db/sequelize')
const { Op } = require('sequelize')
const auth = require('../../auth/auth') 


module.exports = (app) => {
    app.get('/api/alltasks',auth, (req, res) => {
        if (req.query.title) {
            const title = req.query.title
            return Task.findAll({where: {title: { [Op.like]: `%${title}%`}}}) // The findAll method will search all the data 
            .then(tasks => {
                const message = `There are ${tasks.length} that match the search ${title}.`
                res.json({ message, data: tasks })
            })
        }
        Task.findAll({order:['title']})
            .then(tasks => {
                const message = 'The task list has been successfully retrieved.'
                res.json({message, data: tasks})
            })
            .catch(error => {
                const message = "The task list could not be retrieved. Please try again in a few moments. "
                res.status(500).json({message, data:error})
            })
        
    })
}

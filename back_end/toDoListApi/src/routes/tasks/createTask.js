// End-point creation of tasks
const { Task } = require('../../db/sequelize');
const auth = require('../../auth/auth') 
const { ValidationError, UniqueConstraintError } = require('sequelize')

module.exports = (app) => {
    app.post('/api/createtask',auth, (req, res) => {
        // console.log(req.body)
        Task.create(req.body)
            .then(task => {      
                // console.log( task)
                const message = `The task has been successfully created.`
                res.json({ message, data: task })
            })
            .catch(error => {
                if(error instanceof ValidationError) {
                    return res.status(400).json({message: error.message, data: error})
                }
                if(error instanceof UniqueConstraintError){
                    return res.status(400).json({message: error.message, data: error})
                }
                const message = `The task could not be added. Please try again in a few moments.`
                res.status(500).json({message, data: error})
            })
        })
}



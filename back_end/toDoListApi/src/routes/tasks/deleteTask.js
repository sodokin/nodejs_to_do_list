// End-point to delete tasks
const { Task } = require('../../db/sequelize');
const auth = require('../../auth/auth') 


module.exports = (app) => {
    app.delete("/api/deletetasks/:id", (req, res) => {
        Task.findByPk(req.params.id).then( task => { // The findByPk method will allow to identify the precise data to delete
            if (task === null) {
                const message = `The task you want to delete does not exist. Try again with another task.`
                return res.status(404).json({ message })
            }
            const taskDeleted = task;
            return Task.destroy({ // The destroy method will delete the data that was found by the findByPk method
                where: { id: task.id}
            })
            .then(_ => {
                const message = `The task identified in number ${taskDeleted.id} has been deleted.`
                res.json({message, data: taskDeleted})
            })
        })
        .catch(error => {
            const message = `The task could not be deleted. Please try again in a few moments.`
            res.status(500).json({ message, data: error})
        })

    })
}
// End-point to modify tasks
const {Task} = require('../../db/sequelize');
const auth = require('../../auth/auth') 
const { ValidationError, UniqueConstraintError } = require('sequelize')


module.exports= (app) => {
    app.put("/api/updatetasks/:id",auth, (req,res) => {
        const id = req.params.id;
        Task.update(req.body, {
            where: {id: id}
        })
        .then(_=> {
            return Task.findByPk(id).then(task => {
                if (task === null){
                    const message = `The requested task does not exist. Try again with another task.`
                    return res.status(404).json({ message })
                }
                const message = `La tâche ${task.title} a bien été modifiée`
                res.json({message, data: task})
            })
        })
        .catch(error => {
            if(error instanceof ValidationError) {
                return res.status(400).json({message: error.message, data: error})
                }
            if(error instanceof UniqueConstraintError){
                return res.status(400).json({message: error.message, data: error})
                }
            const message = `The task could not be modified. Please try again in a few moments.`
            res.status(500).json({ message, data: error})
        })
    })
}
const { User } = require('../../db/sequelize')
const auth = require('../../auth/auth')


module.exports = (app) => {
    app.get("/todolistapi/showOneUser/:id", (req, res) => {
        User.findByPk(req.params.id)
            .then(user => {
                if (user === null) {
                    const message = "The requested user does not exist. Please create it or select another one.";
                    return res.status(404).json({message});
                }

                const message = `The user ${user.lastname}  has been found !`
                res.json({message, data:user})
            })
            .catch(error => {
                const message = `The user could not be recovered. Try again in a few moments.`
                res.status(500).json({message, data: error})
            })
    })
};
const { User } = require('../../db/sequelize')
const auth = require('../../auth/auth')
const { ValidationError, UniqueConstraintError } = require('sequelize')


module.exports= (app) => {
    app.put("/todolistapi/updateUser/:id", (req,res) => {
        // const id = req.params.id;
        User.update(req.body, {
            where: {id: id}
        })
        .then(_=> {
            return User.findByPk(id).then(user => {
                if (user === null){
                    const message = `The requested user does not exist. Try again with another user.`
                    return res.status(404).json({ message })
                }
                const message = `La tâche ${user.lastname} a bien été modifiée`
                res.json({message, data: user})
            })
        })
        .catch(error => {
            if(error instanceof ValidationError) {
                return res.status(400).json({message: error.message, data: error})
                }
            if(error instanceof UniqueConstraintError){
                return res.status(400).json({message: error.message, data: error})
                }
            const message = `The user could not be modified. Please try again in a few moments.`
            res.status(500).json({ message, data: error})
        })
    })
}
const { User } = require('../../db/sequelize')
const auth = require('../../auth/auth')

module.exports = (app) => {
    app.get('/todolistapi/users',auth, (req, res) => {
        User.findAll({order:['firstname']})
            .then(users => {
                const message = 'The list of users has been successfully retrieved.'
                res.json({message, data: users})
            })
            .catch(error => {
                const message = "The users list could not be retrieved. Please try again in a few moments. "
                res.status(500).json({message, data:error})
            })
        
    })
}
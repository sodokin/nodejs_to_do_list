const { User } = require('../../db/sequelize')
const { ValidationError, UniqueConstraintError } = require('sequelize')

const bcrypt = require('bcrypt');

module.exports = (app) => {
    app.post('/todolistapi/createusers', (req, res) => {
        bcrypt.hash(req.body.password, 10)
            .then(hash => 
                User.create({
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    email: req.body.email,
                    password: req.body.password = hash
                })
                )
            .then(user => { 
                const message = "The users has been successfully created"
                res.json({ message, data: user })
            })

            .catch(error => {
                if(error instanceof ValidationError) {
                    return res.status(400).json({message: error.message, data: error})
                }
                if(error instanceof UniqueConstraintError){
                    return res.status(400).json({message: error.message, data: error})
                }
                const message = `The user could not be added. Please try again in a few moments..`
                res.status(500).json({message, data: error})
            })

    }
)}




const { User } = require('../../db/sequelize');
const auth = require('../../auth/auth') 


module.exports = (app) => {
    app.delete("/todolistapi/deleteuser/:id", (req, res) => {
        User.findByPk(req.params.id).then( user => {
            if (user === null) {
                const message = `La tâche que vous souhaitez supprimer n'existe pas. Réessayez avec une autre tâche.`
                return res.status(404).json({ message })
            }
            const userDeleted = user;
            return User.destroy({
                where: { id: user.id}
            })
            .then(_ => {
                const message = `La tâche identifié au numéro ${userDeleted.id} a bien été supprimé.`
                res.json({message, data: userDeleted})
            })
        })
        .catch(error => {
            const message = `La tâche n'a pas pu être supprimée. Réessayez dans quelques instants.`
            res.status(500).json({ message, data: error})
        })

    })
}
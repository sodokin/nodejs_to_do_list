const { User } = require('../../db/sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config()


module.exports = (app) => {
    app.post('/todolistapi/login', (req, res) => {
        // console.log(req.body)
        User.findOne({ where: {email: req.body.email} }).then(user => {
            if(!user) {
                const message = `This user not exist!`
                return res.status(404).send({message})
            }
            bcrypt.compare(req.body.password, user.password).then(isPasswordValid => {
                if(!isPasswordValid) {
                    const message = `Incorrect password`;
                    return res.status(401).json({  message });
                }
                // Zone du jsonwebtoken
                const token = jwt.sign(
                    {userId: user.id},
                    process.env.PRIVATE_KEY,
                    { expiresIn: '24h'}
                )
                const message = `The user has successfully logged in`;
                return res.json({  message, data: user, token }); // The "Token" must be returned to be used by the client
            })
        })
        .catch(error => {
            const message = `The user has not logged in, Please try again later`;
            return res.json({  message, data: error });
        })
    })
};

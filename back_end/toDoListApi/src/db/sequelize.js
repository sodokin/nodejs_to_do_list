const { Sequelize, DataTypes } = require('sequelize')
const UsersModel = require('../models/userModel')
const TasksModel = require('../models/tasksModel')
// const bcrypt = require('bcrypt');


const sequelize = new Sequelize('nodetododb', 'koudjo', 'koudjo89', {
    host: 'localhost',
        dialect: 'postgres',
        dialectOptions: {
            timezone: 'Etc/GMT-2'
        },
        logging: false
    })
  // Here after having installed postgres by doing npm install --save pg, we create an instance of sequelize which will allow to connect to the database
  // Note that you have to create the database in postgres by hand to be able to connect it 
sequelize.authenticate()
    .then(_ => console.log("the connection to the database has been established"))
    .catch(error => console.error(`Unable to connect to the database ${error}`))

const User = UsersModel(sequelize, DataTypes)
const Task = TasksModel(sequelize, DataTypes)
// sequelize.sync({force:true})
//     .then(_ => console.log(`La base de donnée a bien été synchronisée !`))

const initDb = async() => {
    if (User.username !== undefined && User.password !== undefined) {
        return await sequelize.sync().then(_ => {
            User.create({
                firstname: firstname,
                lastname: lastname,
                email: email,
                password: password
            })
            .then(user => console.log(user.toJSON()))
            .then(_ => console.log("The user has been created !"));

        })
    }
    if (Task.title !== undefined && Task.autor !== undefined && Task.content !== undefined) {
        return await sequelize.sync().then(_ => {
            Task.create({
                title: title,
                autor:  autor,
                content: content
            }).then(task => console.log(`The task has been succesfully created ${task.toJSON()} `))
        })
    }

}

module.exports = {
    initDb, User, Task
}

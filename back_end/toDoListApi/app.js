const express = require('express')
const sequelize = require('./src/db/sequelize');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express()
const port = 3010


app
    .use(morgan("dev"))
    .use(bodyParser.json())
    .use(cors())


sequelize.initDb()
                            // LES ROUTES POUR LES USERS
require('./src/routes/user_log/register')(app)
require('./src/routes/user_log/login')(app)
require('./src/routes/user_log/showUsers')(app)
require('./src/routes/user_log/updateUser')(app)
require('./src/routes/user_log/showOneUser')(app)
require('./src/routes/user_log/deleteUser')(app)

                            //ICI LES POUR LES DIFFERENTES TACHES DE LA TODOLISTE
require('./src/routes/tasks/allTasks')(app)
require('./src/routes/tasks/createTask')(app)
require('./src/routes/tasks/updateTask')(app)
require('./src/routes/tasks/deleteTask')(app)
require('./src/routes/tasks/oneTask')(app)

// // Pour gérer les erreurs 404 , on ne peut pas se servir des middlewares d'express
// // Une bonne solution est de créer une fonction de middlewares à la suite des routes.
app.use(({res}) => {
    const message = "Unable to find the requested resource! You can try another URL."
    res.status(404).json({message})
    }
)


app.listen(port, () => console.log(`listening on port : http://localhost:${port}`));

import axios from "../axiosConfig/axiosConfigdefault"

const INITIAL_STATE = {
    userState: []
}
export default function UserReducer( state = INITIAL_STATE , action ) {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case "LOADUSERS": {
            return {
                ...state,
                userState: action.payload
            }
        }
    }
    // console.log(state)
    return state;
}


export const getUsers =  (token) => async dispatch => {
    try {
        await axios.get("/todolistapi/users", {
            headers: { Authorization: `Bearer ${token}` }
            })
            .then(res => res.data)
            .then((data) => {
                dispatch({
                    type: 'LOADUSERS',
                    payload: data.data
                })
                    
            })

    } catch (error){
        console.log(error.code)
    }

    }


    // axios.post('http://localhost:3010/todolistapi/login',
    //     { firstname: "firstname", lastname: "lastname", email: "email", password: "password"},
    //     {headers: { "Content-type": "application/json" }}
    //     )
    //     .then((res) => res.data)
    //     .then((res) => {
    //         console.log(res);
    //         return res.token
    //     })
    //     .then((token) => fetchUserlist(token))

    // const fetchUserlist = (token) => {
    //     return axios.get("http://localhost:3010/todolistapi/users", {
    //                     headers: { Authorization: `Bearer ${token}` }
    //                 })
    //                 .then(res => res.data)
    //                 .then((data) => {console.log(data)
    //                     dispatch({
    //                         type: 'LOADUSERS',
    //                         payload: data.data
    //                     })
                            
    //                 })
    // }

    // fetch("http://localhost:3010/todolistapi/users",
    //     {headers: { Authorization: `Bearer ${token}` }}
        
    // )
    //     .then((res) => res.json())
    //     .then( data => {
    //         dispatch({
    //         type: 'LOADUSERS',
    //         payload: data.data
    //         })

    //         });


    // }



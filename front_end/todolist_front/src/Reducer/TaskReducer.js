import axios from "../axiosConfig/axiosConfigdefault"


const INITIAL_STATE = {
    taskState : []
}

export default function TaskShowerReducer(state = INITIAL_STATE, action) {
    // eslint-disable-next-line default-case
    switch(action.type) {
        // case "ADDTASKS":
        //     const newTaskState = [...state.taskState]
        //     newTaskState.unshift(action.payload)
        //     return {
        //         taskState: newTaskState
        //     }
        case "SHOWTASKS": {
            return {
                ...state,
                taskState: action.payload
                
            }
        }
    }
    return state;
}

export const getTasks = (token) => async dispatch => {
    
    await axios.get(`/api/alltasks`, {headers: { Authorization: `Bearer ${token}` }})
        .then((res) => res.data)
        .then( data => {
            console.log(data.data)
            dispatch({
                type: 'SHOWTASKS',
                payload: data.data
        })
    })

}

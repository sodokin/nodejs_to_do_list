import axios from "axios";

const BASE_URL = "http://localhost:3010"
// axios.defaults.baseURL = "http://localhost:3010/todolistapi/";
export default axios.create({
    baseURL: BASE_URL
})
// axios.defaults.headers.common['Authorization'] = "AUTH_TOKEN";
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

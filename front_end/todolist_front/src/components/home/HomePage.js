import { useContext } from "react"
import { UserContext } from "../../ContextApi/userContext"

export default function PublicHomePage() {
    const { isUserLoggedIn } = useContext(UserContext)

    return (
        <div className='container p-5'>
            <h1 className="display-3 ">
                {isUserLoggedIn ? 'Bienvenue ! Vous êtes connecté en tant utilisateur inscrit ' :"Bienvenue, vous pouvez vous inscrire ou vous connectez si vous avez déjà un compte"}
            </h1>
        </div>
    )
}







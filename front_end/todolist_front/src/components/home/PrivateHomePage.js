import React from 'react'
import AllTasks from "../Private/showAllTasks/ShowTasks"
import { Link } from 'react-router-dom'


export default function PrivateHomePage() {
    return (
        <div className='container p-5'>
            <h1>Bienvenue sur ce site</h1>
            <AllTasks/>
            <Link to="/private/createTask" > Créer une nouvelle tâche </Link>

        </div>
    )
}

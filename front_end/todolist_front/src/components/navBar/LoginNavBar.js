import { NavLink } from "react-router-dom"
import { useContext } from "react"
import { UserContext } from "../../ContextApi/userContext"
import "./Navbar.css"

export default function NavbarLogin() {
    const {toggleModals,replaceDefaultValues} = useContext(UserContext)

    return (
        <div>
            <nav className="navbar navbar-light bg-light px-4" id="navbarid">
                <NavLink to="/" style={({isActive}) => {return isActive ? {color: "blueviolet"} : {color: ""}}} className="navbar-brand">Home</NavLink>
                <div>
                    <NavLink to="/private/createTask" style={({isActive}) => {return isActive ? {color: "blueviolet"} : {color: ""}}} className="navbar-brand" >Create Tasks</NavLink>
                    <NavLink to="/private/userList"  style={({isActive}) => {return isActive ? {color: "blueviolet"} : {color: ""}}} className="navbar-brand" >Users List</NavLink>
                    <NavLink to="/private/private-home-page"  style={({isActive}) => {return isActive ? {color: "blueviolet"} : {color: ""}}} className="navbar-brand" >Tasks List</NavLink>
                </div>

                <div>
                    <button onClick={() => toggleModals("signUp")} className="btn btn-primary" >Register</button>
                    <button onClick={() => toggleModals("signIn")} className="btn btn-primary ms-2">Login</button>
                    <button onClick={replaceDefaultValues.logout} className="btn btn-primary ms-2">Logout</button>
                </div>
                        
            </nav>
        </div>
    )
}

import { useEffect, useContext } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import {UserContext} from "../../../ContextApi/userContext"
import { getTasks } from '../../../Reducer/TaskReducer'
import axios from '../../../axiosConfig/axiosConfigdefault'
import "./showTask.css"

export let id = 0
export default function ShowTasks() {
    const { isUserLoggedIn, token } = useContext(UserContext)

    const {taskState} = useSelector(state => ({
        ...state.TaskReducer
    }) )

    // Ici le state d'une tâche
    const {oneTaskState} = useSelector(state => ({
        ...state.OneTaskShowerReducer
    }) )


    const dispatch = useDispatch()

    console.log(isUserLoggedIn)
    
    useEffect(() => {
        if (taskState.length === 0){
            try {
                dispatch(getTasks(token))

            } catch (error) {
                console.log(error)
            }
        }
        // if (oneTaskState.length === 0){
        //     dispatch(getOneTasks(id))
        // }
    },[taskState.length, dispatch, token,oneTaskState])


    const taskDeleter = async (id) => {
        await axios.delete(`/api/deletetasks/${id}`)
            .then(res => res.data)
            .catch(err => console.log(err))
    }
    
    // const getOneTasks = async (id, token) => {    
    //     await axios.get(`/api/findOnetask/${id}`, {headers: { Authorization: `Bearer ${token}` }})
    //         .then((res) => res.data)
    //         .then( data => {
    //             return data.data
    //         })
    //         .catch((error) => console.log(error.message))
    
    // }
    // const getId = (id) => {console.log(id)}


    return (
        <div className='taskBody'>
            
            { taskState.map((task) => {
                return (
                    <div className="card border-info mb-3" style={{maxWidth: 90 + "%"}} key={task.id}>
                        <div className="card-header bg-transparent border-info"><h5>Title: {task.title}</h5></div>
                        <div className="card-body text-secondary">
                            <h5 className="card-title">Content</h5>
                            <p className="card-text"> {task.content} </p>
                        </div>
                        <div className="card-footer bg-transparent border-info"><strong>Autor: </strong>{task.autor}</div>

                        <div>
                            <button className='btn btn-danger' style={{maxWidth: 10 + 'rem'}} onClick={() => {taskDeleter(task.id)}}>supprimer</button>
                        </div>
                        <div>
                            <button className='btn btn-info' style={{maxWidth: 10 + 'rem'}}>
                                <Link to="/private/onetask" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Afficher l'article</Link>
                            </button>
                        </div>
                        <div>
                            <button className='btn btn-success' style={{maxWidth: 10 + 'rem'}}>
                                <Link to="/private/update-task" state={{id:task.id, title:task.title, content:task.content, autor:task.autor}} >Modifier l'article </Link>
                            </button>
                        </div>

                    </div>





                    // <>
                    //     <div className="card"   key={task.id} >
                    //         <div className="card-body">
                    //             <h5 className="card-title">Title: {task.title}</h5>
                    //             <p className="card-text"><strong>Content: </strong>{task.content}</p>
                    //             <OneTaskButton />
                    //             <Link className="btn btn-primary" to="/">Afficher la tâche</Link>
                    //         </div>
                    //     </div>
                    //         <button  onClick={() => {taskDeleter(task.id)}}>supprimer</button>
                    // </>
                        // <div key={task.id}>
                        //     <h3>
                        //         Title: {task.title}
                        //     </h3> 
                        //     <p > <strong>autor: </strong>{task.autor} </p>
                        //     <p> <strong>Content: </strong>{task.content} </p>
                        //     <button className='btn btn-danger' onClick={() => {taskDeleter(task.id)}}>supprimer</button>
                        //     <hr/>
                        // </div>
            )
        }) }

        </div>
    )
}



























// import { useEffect } from 'react'
// import { useSelector, useDispatch } from 'react-redux';
// import { Link } from "react-router-dom"
// // import Axios from "axios"

// import { getTasks } from '../../Reducer/TaskReducer'
// // import './showAllTasks.css'
// // import {v4 as uuidv4} from 'uuid'


// export default function AllTasks() {

//     const {taskState} = useSelector( state => ({
//         ...state.TaskReducer
//     }));
//     const dispatch = useDispatch();

//     useEffect(() => {
//         if (taskState.length === 0) {
//             dispatch(getTasks())
//         }
//     }, [taskState.length, dispatch]);
//     // console.log(taskState)

//     // const taskDeleter = (id) => {
//     //     Axios.delete(`http://localhost:3010/api/deletetasks/${id}`)
//     //         .then(res => console.log(res))
//     // }

//     return (
//         <>
//             <h1>Bienvenue sur la page des tâches</h1>
//             { taskState.map(task => {
//                 return (
//                     <div key={task.id}>
//                         <h3>
//                             {task.title}
//                         </h3> 
//                         <p > {task.autor} </p>
//                         <p> {task.content} </p>
//                         {/* <button onClick={() => {taskDeleter(task.id)}}>supprimer</button> */}
//                         <hr/>
//                     </div>
//                 )
//             }) }
//             <button><Link to="/createtasks" >Ajouter une tâche</Link></button>
//         </>
//     )
// }
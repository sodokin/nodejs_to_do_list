import React, {useState, useEffect, useContext} from 'react'
import { useLocation, Link } from 'react-router-dom'
import { UserContext } from "../../../ContextApi/userContext"
import axios from '../../../axiosConfig/axiosConfigdefault'

export default function TasksUpdator() {
    const [taskUpdatingState, setTaskUpdating] = useState({})
    const { token, isUserLoggedIn } = useContext(UserContext)


    const location = useLocation()
    // console.log(location.state.id)

    const handleForm = async (event) => {
        event.preventDefault();
        await axios.post(`/api/updatetasks/${location.state.id}`, taskUpdatingState, {headers: { Authorization: `Bearer ${token}` }})
            .then((res) => res.data)
            .catch((error) => console.log(error))
    }

    // useEffect(() => {
    // }, [])

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setTaskUpdating(values => ({...values, [name]: value}));

    }


    return (
        <div>
            <form className="container" id="taskForm" onSubmit={handleForm}>
                <div className="mb-3 mt-3">
                    <label htmlFor="title" > Titre </label>
                    <input name="title" className="form-control form-control-lg" value={taskUpdatingState.title || location.state.title } onChange={handleChange} type="text" placeholder="Entrez le titre de votre tâche " />
                </div>

                <div className="mb-3">
                    <label htmlFor="autor"> Auteur </label>
                    <input name="autor" className="form-control form-control-lg"  value={taskUpdatingState.autor || location.state.autor } onChange={handleChange} type="text" placeholder="Entrez votre nom " />
                </div>

                <div className="mb-3">
                    <label htmlFor="content"> Détails de la tâche </label>
                    <textarea className="form-control" rows="5" id="comment" name="content" value={taskUpdatingState.content || location.state.content } onChange={handleChange} type="text" placeholder="Expliquez votre tâche " ></textarea>
                </div>

                <button type="submit" className="btn btn-success">Modifier l'article</button>
            </form>

            <Link to="/private/private-home-page">Retour à la liste des tâches</Link>
        </div>
    )
}

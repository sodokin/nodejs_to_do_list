import { useEffect, useContext } from "react"
import { useDispatch, useSelector } from "react-redux";
import { UserContext } from "../../ContextApi/userContext";
import { getOneUsers } from "../../Reducer/OneUserReducer"
// import { UserReducer } from "../../Reducer/LoginReducer";
import axios from "axios";


export default function OneUsersComponent() {
    const { token, isUserLoggedIn, userId } = useContext(UserContext)
    const { oneUserState } = useSelector(state => (
        {...state.OneUserReducer}
    )
    )
    console.log(isUserLoggedIn)
    const dispatch = useDispatch();


    useEffect(() => {
        if(oneUserState.length === 0) {
            try {
                dispatch(getOneUsers(token, userId))
            } catch(error) {
                console.log(error)
            }
        }
    }, [token, dispatch, oneUserState.length, userId ])


    const userDeleter = async (userId) => {
        await axios.delete(`http://localhost:3010/todolistapi/deleteuser/${userId}`)
        .then(res => res.data)

    }


    return (
        <div className='container p-5'>
            
            {/* <ul className="list-group">
                {userState.map(user => {
                    return (
                        <div key={user.id}>
                            <li> 
                                <strong>Lastname:</strong> {user.lastname}
                                <br/>
                                <strong>Firstname:</strong> {user.firstname } 
                                <br/>
                                <strong>Email:</strong> {user.email} 
                                
                            </li>
                            </div>
                            )
                        })}
                        </ul>
                    */}
            <button type="button" className="btn btn-danger" onClick={() => {userDeleter(userId)}}>supprimer</button>
        </div>
    )
}

import { Link,useLocation } from "react-router-dom"



export default function OneTasks() {
    // On utilise useLocation pour récupérer un objet contenant un state qui contient à son tour les données qu'on lui passe dans le fichier ShowTasks
    const location = useLocation()
    console.log(location.state)

    return (
        <>
            <h1>Bienvenue</h1>
            {location.state.title && (
                <div className="card border-info mb-3" style={{maxWidth: 50 + "%"}}>
                    <div className="card-header bg-transparent border-info"> <h4>{location.state.title}</h4> </div>
                    <div className="card-body ">
                        <h5 className="card-title">Content</h5>
                        <p className="card-text">{location.state.content}</p>
                    </div>
                    <div className="card-footer bg-transparent border-info">{location.state.autor}</div>
                </div>
            ) }
            <Link to="/private/private-home-page">Retour à la liste des tâches</Link>
        </>
    )
}



























// import { useEffect } from 'react'
// import { useSelector, useDispatch } from 'react-redux';
// import { Link } from "react-router-dom"
// // import Axios from "axios"

// import { getTasks } from '../../Reducer/TaskReducer'
// // import './showAllTasks.css'
// // import {v4 as uuidv4} from 'uuid'


// export default function AllTasks() {

//     const {taskState} = useSelector( state => ({
//         ...state.TaskReducer
//     }));
//     const dispatch = useDispatch();

//     useEffect(() => {
//         if (taskState.length === 0) {
//             dispatch(getTasks())
//         }
//     }, [taskState.length, dispatch]);
//     // console.log(taskState)

//     // const taskDeleter = (id) => {
//     //     Axios.delete(`http://localhost:3010/api/deletetasks/${id}`)
//     //         .then(res => console.log(res))
//     // }

//     return (
//         <>
//             <h1>Bienvenue sur la page des tâches</h1>
//             { taskState.map(task => {
//                 return (
//                     <div key={task.id}>
//                         <h3>
//                             {task.title}
//                         </h3> 
//                         <p > {task.autor} </p>
//                         <p> {task.content} </p>
//                         {/* <button onClick={() => {taskDeleter(task.id)}}>supprimer</button> */}
//                         <hr/>
//                     </div>
//                 )
//             }) }
//             <button><Link to="/createtasks" >Ajouter une tâche</Link></button>
//         </>
//     )
// }
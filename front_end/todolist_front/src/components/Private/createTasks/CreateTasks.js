// ICI ON FAIT LE TWO WAIST DATA BINDING

import { useState, useContext } from "react"
// import { useDispatch } from "react-redux"
import { UserContext } from '../../../ContextApi/userContext';
import { Link } from "react-router-dom"
import axios from '../../../axiosConfig/axiosConfigdefault'
import "./createTasks.css"

export default function TaskForm() {
    const [taskCreated, setTaskCreated] =  useState({});
    const { token, isUserLoggedIn } = useContext(UserContext)

    console.log(isUserLoggedIn)
    // const dispatch = useDispatch();

    const handleForm = async (event) => {
        event.preventDefault();

        await axios.post("/api/createtask", taskCreated, {headers: { Authorization: `Bearer ${token}` }})
            .then((res) => res.data)

        setTaskCreated({
            title: "",
            autor: "",
            content: ""
        })
    }

    const handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        setTaskCreated(values => ({...values, [name]: value}));
    }
return (
    <>
        <form className="container" id="taskForm" onSubmit={handleForm}>
            <div className="mb-3 mt-3">
                <label htmlFor="title" > Titre </label>
                <input name="title" className="form-control form-control-lg" value={taskCreated.title || ""} onChange={handleChange} type="text" placeholder="Entrez le titre de votre tâche " />
            </div>

            <div className="mb-3">
                <label htmlFor="autor"> Auteur </label>
                <input name="autor" className="form-control form-control-lg"  value={taskCreated.autor || ""} onChange={handleChange} type="text" placeholder="Entrez votre nom " />
            </div>

            <div className="mb-3">
                <label htmlFor="content"> Détails de la tâche </label>
                <textarea className="form-control" rows="5" id="comment" name="content" value={taskCreated.content || ""} onChange={handleChange} type="text" placeholder="Expliquez votre tâche " ></textarea>
            </div>

            <button type="submit" className="btn btn-success">Enregistrez l'article</button>
        </form>
        <Link to="/private/private-home-page" > Retour à la liste des tâche </Link>
    </>
)
}

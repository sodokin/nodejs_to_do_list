import React, {useContext} from 'react'
import { Outlet, Navigate } from 'react-router-dom'
import { UserContext } from '../../ContextApi/userContext'

// Ce composant permet d'accéder aux routes privées si le token est existant

export default function Private() {
    const { isUserLoggedIn } = useContext(UserContext)
    if(!isUserLoggedIn) {
        return <Navigate to="/"/>
    }
    return (
        <div className='container'>
            <Outlet/>
        </div>
    )
}

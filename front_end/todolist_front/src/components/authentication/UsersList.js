import { useEffect, useContext } from "react"
import { useDispatch, useSelector } from "react-redux";
import { UserContext } from "../../ContextApi/userContext";
import { getUsers } from "../../Reducer/LoginReducer"
// import { UserReducer } from "../../Reducer/LoginReducer";
import axios from "axios";


export default function UsersComponent() {
    const { token, isUserLoggedIn } = useContext(UserContext)
    const { userState } = useSelector(state => (
        {...state.UserReducer}
    )
    )
    console.log(isUserLoggedIn)
    const dispatch = useDispatch();


    useEffect(() => {
        if(userState.length === 0) {
            try {
                dispatch(getUsers(token))
            } catch(error) {
                console.log(error)
            }
        }
    }, [token, dispatch, userState.length ])


    const userDeleter = async (id) => {
        await axios.delete(`http://localhost:3010/todolistapi/deleteuser/${id}`)
        .then(res => res.data)

    }


    return (
        <div className='container p-5'>
            
            <ul className="list-group">
                {userState.map(user => {
                    return (
                        <div key={user.id}>
                            <li> 
                                <strong>Lastname:</strong> {user.lastname}
                                <br/>
                                <strong>Firstname:</strong> {user.firstname } 
                                <br/>
                                <strong>Email:</strong> {user.email} 
                                
                            </li>
                            <button type="button" className="btn btn-danger" onClick={() => {userDeleter(user.id)}}>supprimer</button>
                        </div>
                        )
                    })}
            </ul>
        
        </div>
    )
}

import React from 'react'
import { useState, useContext } from "react"
import { UserContext } from '../../../ContextApi/userContext';
import axios from "../../../axiosConfig/axiosConfigdefault"

// import { useNavigate } from 'react-router-dom';


export default function Connection() {
    const [userVerify, setUserVerify] = useState({})
    const { modalState, toggleModals, getUserData, replaceDefaultValues  } = useContext(UserContext)
    const [validation, setValidation] = useState("");

    // const navigate = useNavigate()

    // console.log(replaceDefaultValues)

    const postConnection = async () => {
        try {
            await axios.post("/todolistapi/login", userVerify,
            {headers: { "Content-type": "application/json" }}
                )
            .then((res) => {
                console.log(res.data)
                getUserData(res.data)
                replaceDefaultValues.login(res.data.token, res.data.data.id)
                console.log(res.data.data.lastname)
                // getUserData({"userId": res.data.data.id, "token": res.data.token, "firstname": res.data.data.firstname, "lastname": res.data.data.lastname})
                return res.data.token
                })

        } catch (error) {
            console.log(error)
        }

    }

    const handleVerify = (event) => {
        event.preventDefault();
        postConnection()
            // navigate("/private/private-home-page")
            toggleModals("close")

        setUserVerify({
            email: "",
            password: ""
        })
    }


    const handleVerifyUser = event => {
        const name = event.target.name
        const value = event.target.value
        setUserVerify(values => ({...values, [name]: value}));
    }

    const closeModal = () => {
        setValidation("")
        toggleModals("close")
    }

    return (
        <div>
            {modalState.signInModal && (
                <div className='position-fixed top-0 vw-100 vh-100'>
                    <div onClick={closeModal} className='w-100 h-100 bg-dark bg-opacity-75'>
                    </div>
                        <div className='position-absolute top-50 start-50 translate-middle' style={{minWidth: "400px"}}>
                            <div className='modal-dialog'>
                                <div className='modal-content'>
                                    <div className='modal-header'>
                                        <h5 className='modal-title'> Login </h5>
                                        <button onClick={closeModal } className='btn-close'></button>
                                    </div>

                                    <div className='modal-body'>
                                        <form className='sign-in-form' onSubmit={handleVerify}>

                                            <div className='mb-3'>
                                                <label htmlFor='signInEmail'  className='form-label'>Email</label>
                                                <input type="email" name="email" value={userVerify.email} onChange={handleVerifyUser} required className='form-control' id='signInEmail'/>
                                            </div>

                                            <div className='mb-3'>
                                                <label htmlFor='signInPwd' className='form-label' >Password</label>
                                                <input type="password" name="password" value={userVerify.password} onChange={handleVerifyUser} required className='form-control' id='signInPwd'/>
                                                <p className='text-danger mt-1'>{ validation }</p>
                                            </div>
                                            <button type='submit' className='btn btn-success'>Connect</button>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>

                </div>
            )}

        </div>
    )
}

// export const getToken = (theToken) => {
//     const token = theToken
//     console.log(token)
// }



// import React from 'react'
// import { useState,useRef, useContext } from "react"
// import { UserContext } from '../../../Redux/userContext';
// import axios from "axios"


// export default function Connection() {
//     const [userVerify, setUserVerify] = useState({})
//     const { modalState, toggleModals } = useContext(UserContext)
//     const loginData = useRef([])
//     const getLoginData = elt => {
//         if(elt && !loginData.current.includes(elt) ) {
//             loginData.current.push(elt)
//         }

//     }

//     const handleVerify = async (event) => {
//         event.preventDefault();
//         console.log(loginData.current[0].value)
//         console.log(loginData.current[1].value)

//         try {
//             await axios.post("http://localhost:3010/todolistapi/login", userVerify)
//                 .then((res) => {console.log(res)})
//         } catch (error) {
//             console.log(error)
//         }

//         // dispatch({
//         //     type: 'ADDTASKS',
//         //     payload: taskCreated
//         // })

//         // setUserVerify({
//         //     email: "",
//         //     password: ""
//         // })
//     }


//     const handleVerifyUser = event => {
//         setUserVerify(loginData.current[0].value,
//             loginData.current[1].value  )

//         // const name = event.target.name;
//         // const value = event.target.value;
//         // setUserVerify(values => ({...values, [name]: value}));
//     }
//     console.log(userVerify)
//     return (
//         <div>
//             {modalState.signInModal && (
//                 <div className='position-fixed top-0 vw-100 vh-100'>
//                     <div onClick={() => toggleModals("close") } className='w-100 h-100 bg-dark bg-opacity-75'>
//                     </div>
//                         <div className='position-absolute top-50 start-50 translate-middle' style={{minWidth: "400px"}}>
//                             <div className='modal-dialog'>
//                                 <div className='modal-content'>
//                                     <div className='modal-header'>
//                                         <h5 className='modal-title'> Login </h5>
//                                         <button onClick={() => toggleModals("close") } className='btn-close'></button>
//                                     </div>

//                                     <div className='modal-body'>
//                                         <form className='sign-in-form' onSubmit={handleVerify}>

//                                             <div className='mb-3'>
//                                                 <label htmlFor='signInEmail'  className='form-label'>Email</label>
//                                                 <input ref={getLoginData} type="email" name="email" onChange={handleVerifyUser} required className='form-control' id='signInEmail'/>
//                                             </div>

//                                             <div className='mb-3'>
//                                                 <label htmlFor='signInPwd' className='form-label' >Password</label>
//                                                 <input ref={getLoginData} type="password" name="password" onChange={handleVerifyUser} required className='form-control' id='signInPwd'/>
//                                             </div>
//                                             <button type='submit' className='btn btn-success'>Connect</button>
//                                         </form>

//                                     </div>

//                                 </div>

//                             </div>
//                         </div>

//                 </div>
//             )}

//         </div>
//     )
// }

import React from 'react'
import { useState,useRef, useContext } from "react"
import { UserContext } from '../../../ContextApi/userContext';
import { useNavigate } from 'react-router-dom';
import axios from "../../../axiosConfig/axiosConfigdefault"


export default function Register() {
    const [userCreated, setUserCreated] =  useState({});
    const [validation, setValidation] = useState("");
    const { modalState, toggleModals } = useContext(UserContext);

    const formRef = useRef()
    const navigate = useNavigate()

    const registerPostRequest = async () => {
        try {
            await axios.post("/todolistapi/createusers", userCreated)
                .then((res) => res.data)
                .then((data) => console.log(data))
            // formRef.current.reset()
        } catch (error) {
            if(error.name === "AxiosError"){
                setValidation(error.response.data.message)
                }

            if (error.response.data.data.name === "SequelizeUniqueConstraintError"){
                setValidation(error.response.data.message)
            }
            console.log(error.name)
            console.log(error)
        }

    }

    const handleForm = async (event) => {
        event.preventDefault();
        if( userCreated.password.length <= 6) {
            setValidation("Le mot de passe doit contenir 6 caractères minimum")
            return;
        }
        registerPostRequest()
        setValidation(" ")
        toggleModals("close")
        navigate("/")




        setUserCreated({
            lastname: "",
            firstname: "",
            email: "",
            password: ""
        })
    }

    const handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        setUserCreated(values => ({...values, [name]: value}));
    }
    const closeModal = () => {
        setValidation("")
        toggleModals("close")
    }

    return (
        <>
            {modalState.signUpModal && (
                <div className='position-fixed top-0 vw-100 vh-100'>
                    <div onClick={closeModal } className='w-100 h-100 bg-dark bg-opacity-75'>
                    </div>
                        <div className='position-absolute top-50 start-50 translate-middle' style={{minWidth: "400px"}}>
                            <div className='modal-dialog'>
                                <div className='modal-content'>
                                    <div className='modal-header'>
                                        <h5 className='modal-title'> Register </h5>
                                        <button onClick={closeModal} className='btn-close'></button>
                                    </div>

                                    <div className='modal-body'>
                                        <form ref ={formRef} className='sign-up-form' onSubmit={handleForm}>

                                            <div className='mb-3'>
                                                <label htmlFor='signUpLastname' className='form-label' >Lastname</label>
                                                <input type="text" name="lastname" value={userCreated.lastname || ""} onChange={handleChange} required className='form-control' id='signUpLastname'/>
                                            </div>

                                            <div className='mb-3'>
                                                <label htmlFor='signUpFirstname' className='form-label' >Firstname</label>
                                                <input type="text" name="firstname" value={userCreated.firstname || ""} onChange={handleChange} required className='form-control' id='signUpFirstname'/>
                                            </div>

                                            <div className='mb-3'>
                                                <label htmlFor='signUpEmail'  className='form-label' >Email</label>
                                                <input type="email" name="email" value={userCreated.email || ""} onChange={handleChange} required className='form-control' id='signUpEmail'/>
                                            </div>

                                            <div className='mb-3'>
                                                <label htmlFor='signUpPwd' className='form-label' >Password</label>
                                                <input type="password" name="password" onChange={handleChange} required className='form-control' id='signUpPwd'/>
                                                <p className='text-danger mt-1'>{ validation }</p>
                                            </div>
                                            <button type='submit' className='btn btn-success'>Create Account</button>
                                        </form>

                                    </div>

                                </div>

                            </div>
                        </div>

                </div>
            )}
        </>
    )
}

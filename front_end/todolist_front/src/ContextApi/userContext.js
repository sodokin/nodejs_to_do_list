import {createContext, useState} from 'react'

const defaultValue = {
    token: "",
    userId: 0,
    isUserLoggedIn: false,
    login: () => {}, //Ici login correspond à la fonction getCurrentUserData
    logout: () => {} //Ici logout correspond à la fonction logoutFunc
}
export const UserContext = createContext(defaultValue)
// Ici on vérifie si le token du localStorage existe grace à la fonction getItem
const localStorageToken = localStorage.getItem('currentToken')

export  function UserContextProvider(props) {
    const [currentUserData, setCurrentdata ] = useState({})
    const [userId, setUserId] = useState()
    const [token, setCurrentToken] = useState(localStorageToken)
    // console.log(currentUserData)

    // Fonction pour récupérer les données reçues après la connexion puis les envoyer vers currentUserData
    const getUserData = (data) => {
        setCurrentdata(data)
    }

    // Cette fonction permet de mettre les données récupérées à l'intérieur de leurs state
    const getCurrentUserData = (token, userId) => {
        setCurrentToken(token)
        setUserId(userId)
        // Pour que le token ne disparaît pas lors du rechargement de la page, il faut l'enregistrer dans le local storage de la page web
        // On peut le faire en utilisant la fonction setItem de localStorage
        // Il faut ensuite supprimer l'enregistrement grace à la fonction removeItem de localStorage, dans logoutFunc
        localStorage.setItem("currentToken", token)
    }
    // Cette fonction permet de se déconnecter, pour cela il faut faire passer le token à null
    const logoutFunc = () => {
        setCurrentToken(null)
        localStorage.removeItem("currentToken")
    }



    // Ici on vérifie si le token est true alors on est logué mais on l'initialise à false
    // console.log(token)
    const isUserLoggedIn = !!token;

// Cette variable permet de récupérer les données depuis la page de connexion
    const replaceDefaultValues = {
        token,
        userId,
        isUser: isUserLoggedIn,
        login: getCurrentUserData,
        logout: logoutFunc
    }





    // Ici on crée une fonction et un state qui permettent de gerer la fenêtre modal qui s'ouvre lorsqu'on clique sur se connecter ou s'enregistrer
    const [modalState, setModalState] = useState({
        signUpModal: false,
        signInModal: false
    })
    const toggleModals = modal => {
        if (modal === "signIn") {
            setModalState({
                signUpModal: false,
                signInModal: true
            })
        } else if (modal === "signUp") {
            setModalState({
                signUpModal: true,
                signInModal: false
            })
        }else if (modal === "close"){
            setModalState({
                signUpModal: false,
                signInModal: false
            })
        }
    }


    return (
        <UserContext.Provider value={{modalState, toggleModals, token, getUserData, currentUserData,replaceDefaultValues, isUserLoggedIn }}>
            {props.children}
        </UserContext.Provider>
    )
}

import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from 'redux-thunk'
import TaskReducer from '../Reducer/TaskReducer';
import UserReducer from '../Reducer/LoginReducer';

const rootReducer = combineReducers({ TaskReducer, UserReducer });
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;



// import { configureStore } from "@reduxjs/toolkit"
// // import thunk from 'redux-thunk'
// import TaskShowerReducer from "../Reducer/TaskReducer"
// import LoginReducer from '../Reducer/LoginReducer';

// // const rootReducer = combineReducers({ TaskReducer, LoginReducer });
// const store = configureStore({
//     reducer: {
//         task: TaskShowerReducer,
//         user: LoginReducer
//     }
// })  //, applyMiddleware(thunk));

// export default store;

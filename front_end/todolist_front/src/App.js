import { Routes, Route} from "react-router-dom"
import NavbarLogin from "./components/navBar/LoginNavBar"
import Register from "./components/authentication/register/RegisterPage"
import TaskForm from "./components/Private/createTasks/CreateTasks"
import Connection from "./components/authentication/connection/ConnectionPage"
import PublicHomePage from "./components/home/HomePage"
import OneTask from "./components/Private/showOneTask/ShowOneTask"
import TasksUpdator from "./components/Private/updateTasks/UpdateTasks"
import PrivateHomePage from "./components/home/PrivateHomePage"
import Private from "./components/Private/Private"
import UsersComponent from "./components/authentication/UsersList"


function App() {
  return (
    <div className="App">
      <Register/>
      <Connection/>
      <NavbarLogin/>

      <Routes>
        
        {/* LES ROUTES NON PRIVEE */}
        <Route path="/" element={<PublicHomePage/>}/>


        {/* LES ROUES PRIVEE */}
        <Route path="/private" element= {<Private/>}>
          <Route path="/private/createTask" element={<TaskForm/>} />
          <Route path="/private/userList" element= {<UsersComponent/>} />
          <Route path="/private/private-home-page" element= {<PrivateHomePage/>} />
          <Route path="/private/onetask" element={<OneTask/>}/>
          <Route path="/private/update-task" element={<TasksUpdator/>}/>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
